package com.example.webservice.parsers;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.example.webservice.models.Contacto;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import android.util.Log;


public class ContactParser {
	
	
	
	public ContactParser() {
		
	}
	
	public Contacto[] paserseJson(String json) 
	{
		JsonParser parser = new JsonParser(); 
		Gson gson = new Gson(); 
		JsonElement element = parser.parse(json);// Parsing the string to Json
		JsonObject object = element.getAsJsonObject(); 
		JsonElement ContactosJSON = object.get("contacts"); //getting the contacts 
		Contacto[] contacos = gson.fromJson(ContactosJSON, Contacto[].class); //generating the contacts objects
		return contacos;
	}

}

package com.example.webservice.task;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.example.webservice.models.Contacto;
import com.example.webservice.parsers.ContactParser;

import android.os.AsyncTask;
import android.util.Log;

public class HttpGetter extends AsyncTask<Void, Void, Contacto[]>{
	
	

	@Override
	protected Contacto[] doInBackground(Void... params) {
		Contacto[] contactos = null;
		try
		{
			
			HttpClient cliente = new DefaultHttpClient();
			HttpGet GET = new HttpGet("http://dl.dropboxusercontent.com/u/5025198/contacts.json");
			HttpResponse respuesta = cliente.execute(GET);
			StatusLine statusline = respuesta.getStatusLine();
			if(statusline.getStatusCode() == HttpStatus.SC_OK )
			{
				HttpEntity ent = respuesta.getEntity();
				String data = EntityUtils.toString(ent);
				contactos= new ContactParser().paserseJson(data);
				return contactos;
			}
		}
		catch(ClientProtocolException e)
		{
			Log.i("Respuesta JSON","FAIL");
		}
		catch(IOException e)
		{
			Log.i("Respuesta JSON","FAIL");
		}
		
		return null;

	}
	
	

}

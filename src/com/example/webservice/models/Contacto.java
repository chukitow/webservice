package com.example.webservice.models;

public class Contacto {
	
	private String id;
	private String Contacto;
	private String Numero;

	
	public Contacto() {
	
	}
	
	public Contacto(String id,String Contacto,String Numero) 
	{
		this.id = id;
		this.Contacto = Contacto;
		this.Numero = Numero;
	}
	
	public void setContacto(String contacto) {
		Contacto = contacto;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public void setNumero(String numero) {
		Numero = numero;
	}
	
	public String getContacto() {
		return Contacto;
	}
	
	public String getId() {
		return id;
	}
	
	public String getNumero() {
		return Numero;
	}
	
	
}
